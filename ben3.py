#!/usr/bin/env python3

print(f'inside ben3.py: {__name__=}')

def main():
    print(f'inside ben3.py: do real stuff')

if __name__ == '__main__':
    main()
