#!/usr/bin/env python3

print(f'inside ben2.py: {__name__=}')

if __name__ == '__main__':
    print(f'inside ben2.py: I was executed (not imported).')
else:
    print(f'inside ben2.py: I was imported.')
