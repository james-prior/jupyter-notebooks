#!/usr/bin/env python3

# pytest --capture=tee-sys --color=yes -f sysexit.py

import sys

import pytest

if False:
    sys.exit({'hello': 17})
    1/0
if False:
    try:
        sys.exit({'hey': 19})
    except SystemExit as e:
        print(f'17 {e=}')
        print(f'18 {dir(e)=}')
        print(f'19 {e.args=}')
        print(f'20 {e.code=}')
        print(f'21 {e.__cause__=}')
        print(f'22 {e.with_traceback=}')
        print(f'23 {e.with_traceback()=}')

if True:
    if False:
        def print(*args, **kwargs):
            pass

    def test_sys_exit():
        with pytest.raises(SystemExit) as e:
            sys.exit()

        print(f'17 {e=}')
        print(f'18 {e.value=}')
        print(f'19 {str(e.value)=}')
        print(f'20 {repr(e.value)=}')
        print(f'21 {e.value.code=}')
        print(f'22 {e.value.args=}')
        print(f'23 {type(e.value.code)=}')
        assert e.value.code == None

        print()
        with pytest.raises(SystemExit) as e:
            sys.exit(None)

        print(f'17 {e=}')
        print(f'18 {e.value=}')
        print(f'19 {str(e.value)=}')
        print(f'20 {repr(e.value)=}')
        print(f'21 {e.value.code=}')
        print(f'22 {e.value.args=}')
        print(f'23 {type(e.value.code)=}')
        assert e.value.code == None

        print()
        with pytest.raises(SystemExit) as e:
            sys.exit('hello world')

        print(f'17 {e=}')
        print(f'18 {e.value=}')
        print(f'19 {str(e.value)=}')
        print(f'20 {repr(e.value)=}')
        print(f'21 {e.value.code=}')
        print(f'22 {e.value.args=}')
        print(f'23 {type(e.value.code)=}')
        assert e.value.code == 'hello world'

        print()
        with pytest.raises(SystemExit) as e:
            sys.exit({'Hey': 19})

        print(f'17 {e=}')
        print(f'18 {e.value=}')
        print(f'19 {str(e.value)=}')
        print(f'20 {repr(e.value)=}')
        print(f'21 {e.value.code=}')
        print(f'22 {e.value.args=}')
        print(f'23 {type(e.value.code)=}')
        assert e.value.code == {'Hey': 19}

        print()
        with pytest.raises(SystemExit) as e:
            sys.exit(['Hay', 17])

        print(f'17 {e=}')
        print(f'18 {e.value=}')
        print(f'19 {str(e.value)=}')
        print(f'20 {repr(e.value)=}')
        print(f'21 {e.value.code=}')
        print(f'22 {e.value.args=}')
        print(f'23 {type(e.value.code)=}')
        assert e.value.code == ['Hay', 17]
        # 1/0
